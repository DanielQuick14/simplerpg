﻿using UnityEngine;


namespace UnityCommunityProject.Code
{
    public interface ICollectibleItem
    {
        int ID { get; }
        ItemType Type { get; set; }
        string Name { get; set; }
        int Weight { get; set; }
        int Damage { get; set; }
        int Armor { get; set; }
        int BlockChance { get; set; }
        int NewPrice { get; set; }
        int SellPrice { get; set; }
        int Durability { get; set; }
        int MaxDurability { get; set; }
        int Quantity { get; set; }
    }
}
