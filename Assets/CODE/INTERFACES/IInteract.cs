﻿public interface IInteract {
	void Interact();
	void EndInteract();
}