﻿namespace UnityCommunityProject
{
    public enum CameraType
    {
        Undefined = -1,
        MapGeneration = 0,
        TopDown = 1,
        ThirdPerson = 2,
        FirstPerson = 3,
        InSaucerView = 4
    }

    public enum ColliderType
    {
        Undefined = -1,
        OuterZone = 0,
        InnerZone = 1
    }

    public enum MessageType
    {
        Undefined = -1,
        NearStructure = 0,
        NoLongerNearStructure = 1,
        EnteredBuilding = 2,
        ExitedBuilding = 3,

        // Gar 04/02/2017 Need to let Camera know these events
        PlayerEnteredSaucer = 4,
        PlayerExitedSaucer = 5,

        // Gar 04/04/2017 Need to let PlayerControl know when player is no longer busy (exited a menu, etc)
        PlayerNoLongerBusy = 6,

        // Gar 04/05/2017 Need to let PlayerControl know when player is busy (entered a menu, etc)
        PlayerIsBusy = 7,
    }

    public enum ObjectType
    {
        Undefined = -1,
        Building = 0,
        ResourceStructure = 1
    }

    public enum ElementalDamageType
    {
        Neutral = 0,
        Fire = 1
    }

    public enum ItemType
    {
        Undefined = -1,
        Money = 0,
        Weapon = 1,
        Armor = 2,
        Shield = 3,
        Food = 4,
        Misc = 5
    }
}