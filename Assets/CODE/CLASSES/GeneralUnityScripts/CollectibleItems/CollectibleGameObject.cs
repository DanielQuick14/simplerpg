﻿using UnityEngine;
using UnityCommunityProject.Code;
using DanielQuick;


namespace UnityCommunityProject
{

    public class CollectibleGameObject : MonoBehaviour, IInteract
    {
        [SerializeField]
        private InputInteractTrigger _trigger;
        [SerializeField]
        CollectibleItem _item; 

        public void Interact()
        {
            // Gar 04/07/2017
            GlobalDataStore.Instance.PlayerInventoryAddItem(_item);

            _trigger.Disable();
            Destroy(gameObject);
        }

        public void EndInteract() { }

        public void DisableInteraction()
        {
            _trigger.Disable();
        }

        public void EnableInteraction()
        {
            _trigger.Enable();
        }

    }

}