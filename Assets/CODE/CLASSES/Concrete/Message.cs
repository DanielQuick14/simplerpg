﻿namespace UnityCommunityProject
{
    public class Message {
        public MessageType mtype;
        public ObjectType otype;
        public string name;
        public string description;
    }
}