﻿using UnityEngine;

namespace UnityCommunityProject.Code
{
    [System.Serializable]
    public class CollectibleItem : ICollectibleItem
    {
        private int _id = GlobalDataStore.Instance.GetNewUniqueItemID();
        [SerializeField]
        private ItemType _type;
        [SerializeField]
        private string _name = "";
        [SerializeField]
        private int _weight;
        [SerializeField]
        private int _damage;
        [SerializeField]
        private int _armor;
        [SerializeField]
        private int _blockchance;
        [SerializeField]
        private int _newprice;
        [SerializeField]
        private int _sellprice;
        [SerializeField]
        private int _durability;
        [SerializeField]
        private int _maxdurability;
        [SerializeField]
        private int _count;

        public int ID
        {
            get
            {
                //if (_id < 1)
                //    _id = GlobalDataStore.Instance.GetNewUniqueItemID();

                return _id;
            }
        }

        public ItemType Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        public int Damage
        {
            get { return _damage; }
            set { _damage = value; }
        }

        public int Armor
        {
            get { return _armor; }
            set { _armor = value; }
        }

        public int BlockChance
        {
            get { return _blockchance; }
            set { _blockchance = value; }
        }

        public int NewPrice
        {
            get { return _newprice; }
            set { _newprice = value; }
        }

        public int SellPrice
        {
            get { return _sellprice; }
            set { _sellprice = value; }
        }

        public int Durability
        {
            get { return _durability; }
            set { _durability = value; }
        }

        public int MaxDurability
        {
            get { return _maxdurability; }
            set
            {
                _maxdurability = value;
                if (_durability == 0)
                    _durability = _maxdurability;
            }
        }

        public int Quantity
        {
            get { return _count; }
            set { _count = value; }
        }
    }
}
