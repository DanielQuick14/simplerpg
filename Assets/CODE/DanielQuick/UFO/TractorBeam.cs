﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TractorBeam : MonoBehaviour {

	[SerializeField]
	private float abductionSpeed = 3f;

	private List<IAbductable> abductables;

	private new Collider collider;

	void Awake() {
		collider = GetComponent<Collider>();
		abductables = new List<IAbductable>();
	}

	public void Activate() {
		transform.parent.localScale = new Vector3(1, 100, 1);
		collider.enabled = true;
		abductables.Clear();
	}

	public void Deactivate() {
		collider.enabled = false;
		transform.parent.localScale = new Vector3(1, 0, 1);
		for(int i = 0; i < abductables.Count; i++) {
			abductables[i].OnCancelAbduction();
		}
		abductables.Clear();
	}

	void LateUpdate() {
		for(int i = abductables.Count - 1; i >= 0; i--) {
			Vector3 position = transform.position;
			Transform abductableTransform = ((Component) abductables[i]).transform;
			position.y = abductableTransform.position.y + abductionSpeed * Time.deltaTime;
			abductableTransform.position = position;

			if(Mathf.Abs(transform.position.y - position.y) < 1.5f) {
				abductables[i].OnSuccessfulAbduction();
				abductables.RemoveAt(i);
			}
		}
	}

	void OnTriggerEnter(Collider other) {
		IAbductable abductable = other.GetComponentInParent<IAbductable>();
		if(abductable != null) {
			abductables.Add(abductable);
			abductable.OnStartAbduction();
		}
	}

	void OnTriggerExit(Collider other) {		
		IAbductable abductable = other.GetComponentInParent<IAbductable>();
		if(abductable != null) {
			abductables.Remove(abductable);
			abductable.OnCancelAbduction();
		}
	}

}
