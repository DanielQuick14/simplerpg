﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace DanielQuick {

	public class Dialogue : MonoBehaviour {

		[SerializeField]
		private bool disablesMovement = true;
		[SerializeField]
		private List<DialogueObject> dialogues;

		public void Begin() {
			if(dialogues.Count > 0) {
				DialogueCanvas.Instance.StartDialogue(this, disablesMovement);
			}
		}

		public void End() {
			DialogueCanvas.Instance.EndDialogue();

			DialogueNPC npc = GetComponent<DialogueNPC>();
			if(npc != null) {
				npc.Reset();
			}
		}

		public DialogueObject GetDialogueObject(int index) {
			index = Mathf.Clamp(index, 0, dialogues.Count - 1);

			return dialogues[index];
		}

		[System.Serializable]
		public class DialogueObject {
			[SerializeField]
			private string previewText;
			[SerializeField]
			private string text;
			public string Text {
				get {
					return text;
				}
			}

			[SerializeField]
			private List<DialogueChoice> choices;

			public List<DialogueChoice> Choices {
				get {
					return choices;
				}
			}

			[System.Serializable]
			public class DialogueChoice {
				[SerializeField]
				private string text;
				public string Text {
					get {
						return text;
					}
				}
				[SerializeField]
				private int nextDialogue = -1;
				public int NextDialogue {
					get {
						return nextDialogue;
					}
				}
				[SerializeField]
				private UnityEvent action;
				public UnityEvent Action {
					get {
						return action;
					}
				}
			}
		}

	}

}