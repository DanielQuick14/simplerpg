﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DanielQuick {
	
	[RequireComponent(typeof(Dialogue))]
	public class DialogueNPC : MonoBehaviour, IInteract {

		private Dialogue dialogue;
		private InputInteractTrigger trigger;

		void Awake() {
			dialogue = GetComponent<Dialogue>();
			trigger = GetComponentInChildren<InputInteractTrigger>();
		}

		public void Interact() {
			dialogue.Begin();
		}

		public void Reset() {
			if(trigger != null) {
				trigger.ResetTrigger();
			}
		}

		public void EndInteract() {}

	}

}