﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityCommunityProject;

namespace DanielQuick {

	public class DialogueCanvas : MonoBehaviour {

		[SerializeField]
		private Transform choicePrefab;

		private Text text;
		private bool movementEnabled = false;
		private Dialogue controller;
		private Transform choicesParent;

		private static DialogueCanvas instance;
		public static DialogueCanvas Instance {
			get {
				return instance;
			}
		}

		void Awake() {
			instance = this;
			text = GetComponentInChildren<Text>();
			choicesParent = transform.Find("Choices");
			gameObject.SetActive(false);
		}

		public void StartDialogue(Dialogue controller, bool disablesMovement) {
			this.controller = controller;

			PlayerControl playerControl = Player.Instance.GetComponent<PlayerControl>();
			movementEnabled = playerControl.enabled;
			if(disablesMovement) {
				playerControl.enabled = false;
			}

			Dialogue.DialogueObject obj = controller.GetDialogueObject(0);
			ShowDialogue(obj);

			gameObject.SetActive(true);
		}

		void ShowDialogue(Dialogue.DialogueObject obj) {
			text.text = obj.Text;

			for(int i = 0; i < choicesParent.childCount; i++) {
				Destroy(choicesParent.GetChild(i).gameObject);
			}

			for(int i = 0; i < obj.Choices.Count; i++) {
				Dialogue.DialogueObject.DialogueChoice choice = obj.Choices[i];
				CreateChoice(choice);
			}

			if(obj.Choices.Count == 0) {
				Invoke("End", 3);
			}
		}

		void CreateChoice(Dialogue.DialogueObject.DialogueChoice choice) {
			Transform instance = Instantiate<Transform>(choicePrefab);
			instance.SetParent(choicesParent, false);
			instance.Find("Text").GetComponent<Text>().text = choice.Text;
			Dialogue.DialogueObject.DialogueChoice curChoice = choice;
			int nextDialogue = choice.NextDialogue;
			instance.GetComponent<Button>().onClick.AddListener(() => 
				OnChoiceSelected(curChoice, nextDialogue)
			);
		}

		void OnChoiceSelected(Dialogue.DialogueObject.DialogueChoice curChoice, int nextDialogue) {
			curChoice.Action.Invoke();

			if(nextDialogue == -1) {
				controller.End();
			} else {
				ShowDialogue(controller.GetDialogueObject(nextDialogue));
			}
		}

		void End() {
			if(controller != null) {
				controller.End();
			}
		}

		public void EndDialogue() {
			Player.Instance.GetComponent<PlayerControl>().enabled = movementEnabled;
			gameObject.SetActive(false);
		}
	}

}