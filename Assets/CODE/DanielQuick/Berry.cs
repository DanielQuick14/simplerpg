﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityCommunityProject.Code;

namespace DanielQuick {

	public class Berry : MonoBehaviour, IInteract {

		[SerializeField]
		private InputInteractTrigger trigger;

        // Gar 04/08/2017
        [SerializeField]
        CollectibleItem _item;


        public void Interact() {
            // Gar 04/07/2017
            GlobalDataStore.Instance.PlayerInventoryAddItem(_item);

			trigger.Disable();
			Destroy(gameObject);
		}

		public void EndInteract() {}

		public void DisableInteraction() {
			trigger.Disable();
		}

		public void EnableInteraction() {
			trigger.Enable();
		}

	}

}