﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DanielQuick {

	public class Bush : MonoBehaviour, IInteract {

		[SerializeField]
		private Berry berryPrefab;
		[SerializeField]
		private float berryRefreshTime = 60;

		[SerializeField]
		private float ejectForce = 2f;
		[SerializeField]
		private Transform ejectOrigin;
		[SerializeField]
		private Transform berryHandleContainer;
		[SerializeField]
		private InputInteractTrigger trigger;

		private bool hasBerries;

		void Awake() {
			GenerateBerries();
		}

		void GenerateBerries() {
			for(int i = 0; i < berryHandleContainer.childCount; i++) {
				Transform berryHandler = berryHandleContainer.GetChild(i);

				Berry berry = Instantiate<Berry>(berryPrefab);
				berry.transform.SetParent(berryHandler);
				berry.transform.localPosition = Vector3.zero;
				berry.GetComponent<Rigidbody>().isKinematic = true;
				berry.DisableInteraction();
			}

			trigger.Enable();
		}

		public void Interact() {
			for(int i = 0; i < berryHandleContainer.childCount; i++) {
				Transform berryHandler = berryHandleContainer.GetChild(i);
				if(berryHandler.childCount > 0) {
					Berry berry = berryHandler.GetChild(0).GetComponent<Berry>();
					berry.transform.SetParent(null);
					berry.GetComponent<Rigidbody>().isKinematic = false;
					berry.GetComponent<Rigidbody>().AddForce((berry.transform.position - ejectOrigin.position).normalized * ejectForce, ForceMode.Impulse);
					berry.EnableInteraction();
				}
			}

			Invoke("GenerateBerries", berryRefreshTime);
			trigger.Disable();
		}

		public void EndInteract() {

		}

	}

}