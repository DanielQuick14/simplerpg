﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DanielQuick {

	public class InputInteractTrigger : InteractTrigger {

		[SerializeField]
		private Transform iconAnchor;

		[SerializeField]
		private KeyCode keyCode = KeyCode.E;

		private bool hasPressedKey = false;
		private bool isInteracting = false;

		protected override void StartInteraction() {
			if(isInteracting) return;

			InteractionCanvas.Instance.AddIcon(iconAnchor, keyCode);
			isInteracting = true;
			hasPressedKey = false;
		}

		protected override void EndInteraction() {
			if(!isInteracting) return;

			InteractionCanvas.Instance.RemoveIcon(iconAnchor);

			base.EndInteraction();

			isInteracting = false;
			hasPressedKey = false;
		}

		void Update() {
			if(isInteracting && !hasPressedKey) {
				if(Input.GetKeyDown(keyCode)) {
					base.StartInteraction();
					InteractionCanvas.Instance.ActivateIcon(iconAnchor);
					hasPressedKey = true;
				}
			}
		}

		public void Disable() {
			EndInteraction();
			GetComponent<Collider>().enabled = false;
		}

		public void Enable() {
			GetComponent<Collider>().enabled = true;
		}

		public void ResetTrigger() {
			isInteracting = false;
			InteractionCanvas.Instance.RemoveIcon(iconAnchor);

			GetComponent<Collider>().enabled = false;
			GetComponent<Collider>().enabled = true;
			hasPressedKey = false;
		}

	}

}