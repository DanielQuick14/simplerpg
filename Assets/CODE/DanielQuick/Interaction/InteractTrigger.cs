﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DanielQuick {

	public class InteractTrigger : MonoBehaviour {

		[SerializeField]
		private GameObject interactableObj;
		private IInteract interactable;

		void Awake() {
			interactable = interactableObj.GetComponent<IInteract>();
		}

		void OnTriggerEnter(Collider other) {
			if(other.CompareTag("Player")) {
				StartInteraction();
			}
		}

		protected virtual void StartInteraction() {
			if(interactable != null) {
				interactable.Interact();
			}
		}

		void OnTriggerExit(Collider other) {
			if(other.CompareTag("Player")) {
				EndInteraction();
			}
		}

		protected virtual void EndInteraction() {
			if(interactable != null) {
				interactable.EndInteract();
			}
		}

	}

}