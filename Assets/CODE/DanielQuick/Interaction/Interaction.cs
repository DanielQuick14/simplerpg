﻿using UnityEngine;
using System.Collections;

namespace DanielQuick {

	/// <summary>
	/// Simple interaction mechanic. Only checks against the "Interactable" layermask
	/// </summary>
	public class Interaction : MonoBehaviour {

		public LayerMask layerMask;

		public static readonly float DEFAULT_DISTANCE = 5f;

		void Update() {
			if(Input.GetMouseButtonDown(0)) {
				RaycastHit hit;
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				if(Physics.Raycast(ray, out hit, layerMask)) {
					IInteract interactableObject = hit.collider.GetComponentInParent<IInteract>();
					if(interactableObject != null) {
						interactableObject.Interact();
					}
				}
			}
		}

	}

}