﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DanielQuick {

	public class Door : MonoBehaviour, IInteract {

		private Animator animator;

		void Awake() {
			animator = GetComponent<Animator>();
		}

		public void Interact() {
			animator.SetBool("IsOpen", true);
		}

		public void EndInteract() {
			animator.SetBool("IsOpen", false);
		}

	}

}