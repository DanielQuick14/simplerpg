﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace UnityCommunityProject.Code
{
    public sealed class GlobalDataStore
    {
        private static readonly GlobalDataStore instance = new GlobalDataStore();

        // every 
        private int _uniqueitemid = 0;

        // stored here for convenience & simplicity
        private readonly List<ICollectibleItem> _playerinventory = new List<ICollectibleItem>();
        private readonly List<ICollectibleItem> _blacksmithinventory = new List<ICollectibleItem>();


        #region PLAYER INVENTORY

        public List<ICollectibleItem> PlayerGetFullInventory()
        {
            return _playerinventory;
        }

        public List<ICollectibleItem> PlayerGetDamagedInventory()
        {
            return _playerinventory.Where(i => i.Durability < i.MaxDurability).ToList<ICollectibleItem>();
        }

        public void PlayerInventoryAddItem(GameObject goItem)
        {
            CollectibleItem item = Translations.Instance.GameObjectToCollectibleItem(goItem);
            PlayerInventoryAddItem(item);
        }

        public void PlayerInventoryAddItem(ICollectibleItem item)
        {
            ICollectibleItem itemdupe = _playerinventory.Where(i => i.Type == item.Type && i.Name == item.Name).FirstOrDefault();

            // if item already exists in inventory, increment the Quantity
            if (itemdupe != null)
            {
                itemdupe.Quantity++;
            }
            // otherwise add it
            else
            {
                if (item.Quantity < 1)
                    item.Quantity = 1;

                _playerinventory.Add(item);
            }
        }

        public ICollectibleItem PlayerInventoryGetItem(int id)
        {
            return _playerinventory.Find(i => i.ID == id);
        }

        public void PlayerInventoryRemoveItem(int id)
        {
            _playerinventory.Remove(_playerinventory.Find(i => i.ID == id));
        }

        #endregion



        #region BLACKSMITH INVENTORY ... we could have an array of various NPC inventories but let's keep things simple & explicit

        public void BlacksmithInitializeInventory(ICollectibleItem[] startingInventory)
        {
            if (startingInventory == null)
                return;

            if (startingInventory.Length < 1)
                return;

            // load up the shop with the initial inventory
            for (int index = 0; index < startingInventory.Length; index++)
                BlacksmithInventoryAddItem(startingInventory[index]);
        }

        public void BlacksmithClearInventory()
        {
            _blacksmithinventory.Clear();
        }


        public List<ICollectibleItem> BlacksmithGetFullInventory()
        {
            return _blacksmithinventory;
        }

        public void BlacksmithInventoryAddItem(ICollectibleItem item)
        {
            ICollectibleItem itemdupe = _blacksmithinventory.Where(i => i.Type == item.Type && i.Name == item.Name).FirstOrDefault();

            // if item already exists in inventory, increment the Quantity
            if (itemdupe != null)
            {
                itemdupe.Quantity++;
            }
            // otherwise add it
            else
            {
                if (item.Quantity < 1)
                    item.Quantity = 1;

                _blacksmithinventory.Add(item);
            }
        }

        public ICollectibleItem BlacksmithInventoryGetItem(int id)
        {
            return _blacksmithinventory.Find(i => i.ID == id);
        }

        public void BlacksmithInventoryRemoveItem(int id)
        {
            _blacksmithinventory.Remove(_playerinventory.Find(i => i.ID == id));
        }

        #endregion


        public int GetNewUniqueItemID()
        {
            return ++_uniqueitemid;
        }






        // singleton

        static GlobalDataStore()
        {
        }

        private GlobalDataStore()
        {
        }

        public static GlobalDataStore Instance
        {
            get
            {
                return instance;
            }
        }
    }

}

