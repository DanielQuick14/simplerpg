﻿// Just to make it simpler and minimize needing to update other people's work this allows translating
// one thing into another... such as GameObjects into CollectibleItems. Think of it as the Adapter Pattern.

using System;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;


namespace UnityCommunityProject.Code
{
    public sealed class Translations
    {
        private static readonly Translations instance = new Translations();



        // Converts a GameObject into a CollectibleItem if possible
        public CollectibleItem GameObjectToCollectibleItem(GameObject goItem)
        {
            CollectibleItem item = new CollectibleItem();

            // we only want the base name not the ( ) and numbers in the case of clones and definitely not "Clone"
//            item.Name = AlphaOnly(goItem.name).Replace("Clone", "");

            // Actually I think these might be better off just processed right here.
            // How many can there be. Even if we have 50 distinct items it is no big deal.
            //item.Type = ConvertGameObjectTagToItemType(goOriginal);

            switch (goItem.tag.ToLower())
            {
                case "redberry":
                    item.Name = "Red Berry";
                    item.Type = ItemType.Food;
                    item.Weight = 1;
                    item.Damage = 0;
                    item.Armor = 0;
                    item.BlockChance = 0;
                    item.NewPrice = 2;
                    item.SellPrice = 0;
                    item.MaxDurability = 1;
                    item.Durability = 1;
                    break;

                case "woodenclub":
                    item.Name = "Wooden Club";
                    item.Type = ItemType.Weapon;
                    item.Weight = 2;
                    item.Damage = 1;
                    item.Armor = 0;
                    item.BlockChance = 5;
                    item.NewPrice = 5;
                    item.SellPrice = 1;
                    item.MaxDurability = 25;
                    item.Durability = 15;
                    break;

                case "goldnugget":
                    item.Name = "Gold Nugget";
                    item.Type = ItemType.Money;
                    item.Weight = 0;
                    item.Damage = 0;
                    item.Armor = 0;
                    item.BlockChance = 0;
                    item.NewPrice = 1;
                    item.SellPrice = 1;
                    item.MaxDurability = 0;
                    item.Durability = 0;
                    break;

                default:
                    throw new System.NotSupportedException("tag '" + goItem.tag + "' could not be converted into an ItemType." + Environment.NewLine + "Check the tag on the GameObject '" + goItem.name + "' and make sure the Translations class's ConvertGameObjectTagToItemType method handles the tag.");
            }

            return item;
        }


        // Converts a GameObject tag into an ItemType enumeration.
        // This means every GameObject that can be collected needs to have a tag that is supported by this method.
        private ItemType ConvertGameObjectTagToItemType(GameObject go)
        {
            ItemType etype;

            switch (go.tag.ToLower())
            {
                case "redberry":
                    etype = ItemType.Food;
                    break;

                case "woodenclub":
                    etype = ItemType.Weapon;
                    break;

                default:
                    throw new System.NotSupportedException("tag '" + go.tag + "' could not be converted into an ItemType." + Environment.NewLine + "Check the tag on the GameObject '" + go.name + "' and make sure the Translations class's ConvertGameObjectTagToItemType method handles the tag.");
            }

            return etype;
        }

        private string AlphaOnly(string data)
        {
            // two ways off the top of my head to do this... need to test for performance to see which way is fastest at some point

            // using regex
            Regex reg = new Regex("[^a-zA-Z' ]");
            reg.Replace(data, "");

            // using linq
            data = new string(data.Where(c => Char.IsLetter(c)).ToArray());

            return data;
        }
        
        
        
        // singleton

        static Translations()
        {
        }

        private Translations()
        {
        }

        public static Translations Instance
        {
            get
            {
                return instance;
            }
        }
    }

}

