﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;


namespace UnityCommunityProject
{
    public class DamageDealer : MonoBehaviour
    {

        [Header("REFERENCES")]
        public GameObject go;

        [Header("VARIABLES")]
        public DamageInfo DamageInfo;

        [Header("FLAGS")]
        public bool tf;

        void Start()
        {

        }

        void Update()
        {

        }

        void OnTriggerEnter(Collider Collider)
        {
            Collider.SendMessage("TakeDamage", DamageInfo);
        }
    }
}