﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityCommunityProject;

namespace FroggyTools
{
    /* Master-Frog */
    [System.Serializable]   
    public class UnitStatSet
    {   
        public int Strength;
        public int Intelligence;
        public int Dexterity;
        public int Agility;
        public int Vitality;
               
        public int HpMax;
        public int HpCurrent;
        public int HpRegenRate;

        public int MpMax;
        public int MpCurrent;
        public int MpRegenRate; 

        public int EnergyMax;
        public int EnergyCurrent;
        public int EnergyRegenRate;

        public int RageMax;
        public int RageCurrent;
        public int RageCoolRate;

        public int ExpRequired;

        public int FireResistance;
        public int IceResistance;

        //public event Action<GameObject> OnDeath;

        /*void Start()
        {
            GiveFullStats();
            OnDeath += DestroyOnDeath;
        }

        public void GiveFullStats()
        {
            HpCurrent = HpMax;
        }

        public void RemoveHP(int amount)
        {
            HpCurrent -= amount;
            if (HpCurrent <= 0)
            {
                if (OnDeath != null)
                    OnDeath(gameObject);
            }
        }

        private void DestroyOnDeath(GameObject killedGameObject)
        {
            Debug.Log(name + " has died, blah.");
            Destroy(killedGameObject);
        }*/
    }
}