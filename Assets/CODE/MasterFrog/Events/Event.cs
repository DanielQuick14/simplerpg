﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FroggyTools {
    [System.Serializable]
    public class Event : MonoBehaviour {

        public FroggyTools.Effect Effect;

        public virtual void Trigger() {}
    }
}
