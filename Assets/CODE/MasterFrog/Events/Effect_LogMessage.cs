﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FroggyTools {
    public class Effect_LogMessage : FroggyTools.Effect {
        public string Message;
        public override void Trigger() {
            Debug.Log(Message);
        }
    }
}
