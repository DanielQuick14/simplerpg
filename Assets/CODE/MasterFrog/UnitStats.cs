﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityCommunityProject;

namespace FroggyTools
{
    public class UnitStats : MonoBehaviour
    {
        public int MaxLevel;
        public int CurrentLevel;
        public int CurrentExperience;
        public UnitStatSet MinStats;
        public UnitStatSet MaxStats;
        public UnitStatSet BaseStats;
        public List<UnitStatSet> EquipmentBonuses;
        public List<UnitStatSet> SpellBuffs;
        public UnitStatSet BonusStats;
        public UnitStatSet CurrentStats;

        public FroggyTools.Effect OnDeathEvent;
        public FroggyTools.Effect OnTakeDamageEvent;

        public bool DestroyOnDeath;

        private float SecondsSinceLastTick;

        void Start()
        {
            GenerateBaseStats();
            GetCurrentStats();
            FullRecovery();
        }

        public void GetCurrentStats() {
            CurrentStats = BaseStats;
        }

        public void GenerateBaseStats()
        {
            float t;

            // So this will turn our current level / maxlevel into a number between 0.0 and 1.0... 
            if (MaxLevel > 1) {
                t = (((float)CurrentLevel - 1) / ((float)MaxLevel - 1));
            } else {
                t = 1;
            }

            BaseStats.Agility = StatLerp(MinStats.Agility, MaxStats.Agility, t);
            BaseStats.Dexterity = StatLerp(MinStats.Dexterity, MaxStats.Dexterity, t);
            BaseStats.Intelligence = StatLerp(MinStats.Intelligence, MaxStats.Intelligence, t);
            BaseStats.Strength = StatLerp(MinStats.Strength, MaxStats.Strength, t);
            BaseStats.Vitality = StatLerp(MinStats.Vitality, MaxStats.Vitality, t);
            
            BaseStats.HpMax = StatLerp(MinStats.HpMax, MaxStats.HpMax, t);
            BaseStats.HpRegenRate = StatLerp(MinStats.HpRegenRate, MaxStats.HpRegenRate, t);

            BaseStats.MpMax = StatLerp(MinStats.MpMax, MaxStats.MpMax, t);
            BaseStats.MpRegenRate = StatLerp(MinStats.MpRegenRate, MaxStats.MpRegenRate, t);

            BaseStats.EnergyMax = StatLerp(MinStats.EnergyMax, MaxStats.EnergyMax, t);
            BaseStats.EnergyRegenRate = StatLerp(MinStats.EnergyRegenRate, MaxStats.EnergyRegenRate, t);

            BaseStats.RageMax = StatLerp(MinStats.RageMax, MaxStats.RageMax, t);
            BaseStats.RageCoolRate = StatLerp(MinStats.RageCoolRate, MaxStats.RageCoolRate, t);

            BaseStats.ExpRequired = StatLerp(MinStats.ExpRequired, MaxStats.ExpRequired, t);
        }

        private int StatLerp(int a, int b, float t)
        {// An easier to read lerp that returns an int
            return Mathf.RoundToInt(Mathf.Lerp((float)a, (float)b, t));
        }

        public void GainExperience(int amount) {
            CurrentExperience += amount;
        }

        public void LevelUp() {
            CurrentLevel += 1;
            GenerateBaseStats();
            FullRecovery();
        }

        public void FullRecovery() {
            CurrentStats.HpCurrent = CurrentStats.HpMax;
            CurrentStats.MpCurrent = CurrentStats.MpMax;
            CurrentStats.EnergyCurrent = CurrentStats.EnergyMax;
        }

        public void TakeDamage (DamageInfo DamageInfo) {
            if (OnTakeDamageEvent != null) {
                OnTakeDamageEvent.Trigger();
            }

            GenerateBaseStats();
            // Factor in all stat bonuses
            CurrentStats.HpCurrent -= DamageInfo.BaseDamage;
            if (CurrentStats.HpCurrent <= 0) {
                CurrentStats.HpCurrent = 0;
                Die();
            }
        }

        private void Die() {
            if (OnDeathEvent != null) {
                OnDeathEvent.Trigger();
            }

            if (DestroyOnDeath) {
                Destroy(this.gameObject);
            }
        }

        void Tick() {
            CurrentStats.HpCurrent += CurrentStats.HpRegenRate;
            CurrentStats.EnergyCurrent += CurrentStats.EnergyRegenRate;
            CurrentStats.MpCurrent += CurrentStats.MpRegenRate;
            CurrentStats.RageCurrent -= CurrentStats.RageCoolRate;
        }

        void Update()
        {
            SecondsSinceLastTick += Time.deltaTime;

            if (SecondsSinceLastTick >= 1) {
                Tick();
                SecondsSinceLastTick = 0;    
            }

            if (CurrentExperience >= CurrentStats.ExpRequired) {
                CurrentExperience -= CurrentStats.ExpRequired;
                LevelUp();
            }

            CurrentStats.HpCurrent = Mathf.Clamp(CurrentStats.HpCurrent, 0, CurrentStats.HpMax);
            CurrentStats.MpCurrent = Mathf.Clamp(CurrentStats.MpCurrent, 0, CurrentStats.MpMax);
            CurrentStats.EnergyCurrent = Mathf.Clamp(CurrentStats.EnergyCurrent, 0, CurrentStats.EnergyMax);
            CurrentStats.RageCurrent = Mathf.Clamp(CurrentStats.RageCurrent, 0, CurrentStats.RageMax);


        }
    }
}