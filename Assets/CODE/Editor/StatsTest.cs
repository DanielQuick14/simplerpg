﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;

namespace UnityCommunityProject.Testing
{
    public class StatsTest
    {
        private void Arrange()
        {
            statsGameObject = new GameObject("Stats Test GameObject");
            testStats = statsGameObject.AddComponent<PlayerStats>();
            testStats.GiveFullStats();
        }

        private GameObject statsGameObject;
        private PlayerStats testStats;

        [Test]
        public void GiveFullStatsMatchesMaximumHP()
        {
            Arrange();

            Assert.AreEqual(testStats.GetMaximumHP(), testStats.GetCurrentHP());
        }

        [Test]
        public void RemoveHP([Random(1, 99, 5)] int amount)
        {
            Arrange();

            int testHealthPoints = testStats.GetCurrentHP() - amount;

            testStats.RemoveHP(amount);

            Assert.AreNotEqual(testStats.GetMaximumHP(), testStats.GetCurrentHP());
            Assert.AreEqual(testHealthPoints, testStats.GetCurrentHP());
        }

        [Test]
        public void CurrentHealthLessThanOrEqualToZeroCallsOnDeathEvent([Values(100, 101)]int amount)
        {
            Arrange();

            bool onDeathCalled = false;

            testStats.OnDeath += (GameObject obj) =>
            {
                onDeathCalled = true;    
            };

            testStats.RemoveHP(amount);

            Assert.IsTrue(onDeathCalled);
        }
    }
}