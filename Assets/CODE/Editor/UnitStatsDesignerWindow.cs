﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace FroggyTools
{
    public class UnitStatsDesignerWindow : EditorWindow
    {
        public UnitStats UnitStats;
        public UnitStatSet UnitStatSet;

        void Init()
        {
            UnitStatSet = UnitStats.MinStats;
        }

        void OnGUI()
        {
            EditorGUILayout.TextArea("", GUI.skin.horizontalSlider);
            EditorGUILayout.LabelField("Now editing stats for [" + UnitStats.gameObject.name + "]", EditorStyles.boldLabel);
            EditorGUILayout.TextArea("", GUI.skin.horizontalSlider);

            EditorGUILayout.BeginVertical();

            var CurrentLevel = EditorGUILayout.IntField(new GUIContent("Current Level",
                "The current level of this unit."),
                UnitStats.CurrentLevel,
                GUILayout.ExpandWidth(false));
            if (CurrentLevel <= UnitStats.MaxLevel && CurrentLevel > 0)
            {
                UnitStats.CurrentLevel = CurrentLevel;
            }

            UnitStats.GenerateBaseStats();

            EditorGUILayout.LabelField("Base Stats for this level:", EditorStyles.boldLabel);

            EditorGUILayout.LabelField("Agility: " + UnitStats.BaseStats.Agility, EditorStyles.boldLabel);
            EditorGUILayout.LabelField("Dexterity: " + UnitStats.BaseStats.Dexterity, EditorStyles.boldLabel);
            EditorGUILayout.LabelField("Intelligence: " + UnitStats.BaseStats.Intelligence, EditorStyles.boldLabel);
            EditorGUILayout.LabelField("Strength: " + UnitStats.BaseStats.Strength, EditorStyles.boldLabel);
            EditorGUILayout.LabelField("Vitality: " + UnitStats.BaseStats.Vitality, EditorStyles.boldLabel);

            EditorGUILayout.LabelField("Maximum HP: " + UnitStats.BaseStats.HpMax, EditorStyles.boldLabel);
            EditorGUILayout.LabelField("Hp Regeneration Rate: " + UnitStats.BaseStats.HpRegenRate + " Hp gained per second", EditorStyles.boldLabel);

            EditorGUILayout.LabelField("Maximum MP: " + UnitStats.BaseStats.MpMax, EditorStyles.boldLabel);
            EditorGUILayout.LabelField("Mp Regeneration Rate: " + UnitStats.BaseStats.MpRegenRate + " Mp gained per second", EditorStyles.boldLabel);

            EditorGUILayout.LabelField("Maximum Energy: " + UnitStats.BaseStats.EnergyMax, EditorStyles.boldLabel);
            EditorGUILayout.LabelField("Energy Regeneration Rate: " + UnitStats.BaseStats.EnergyRegenRate + " energy gained per second", EditorStyles.boldLabel);

            EditorGUILayout.LabelField("Maximum Rage: " + UnitStats.BaseStats.RageMax, EditorStyles.boldLabel);
            EditorGUILayout.LabelField("Rage Cool Rate: " + UnitStats.BaseStats.RageCoolRate + " rage lost per second", EditorStyles.boldLabel);

            EditorGUILayout.LabelField("Experience Required: " + UnitStats.BaseStats.ExpRequired, EditorStyles.boldLabel);

            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical();
  
            HL("Select Max Level");

            UnitStats.MaxLevel = EditorGUILayout.IntField(new GUIContent("Max Level",
                "The highest level that this unit can grow to."), 
                UnitStats.MaxLevel, 
                GUILayout.ExpandWidth(false));

            
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical();
            EditorGUILayout.BeginHorizontal();

            HL("Define Stats for Level 1 and Max Level (" + UnitStats.MaxLevel + ")");

            EditorGUILayout.LabelField("Level 1 Stats", EditorStyles.boldLabel);
            EditorGUILayout.LabelField("|Max Level (" + UnitStats.MaxLevel + ") Stats", EditorStyles.boldLabel);
            
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            UnitStats.MinStats.Agility = EditorGUILayout.IntField(new GUIContent("Agility", "This unit's Agility."), UnitStats.MinStats.Agility);
            UnitStats.MaxStats.Agility = EditorGUILayout.IntField(new GUIContent("| Agility", "This unit's Agility."), UnitStats.MaxStats.Agility);
      
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            UnitStats.MinStats.Dexterity = EditorGUILayout.IntField(new GUIContent("Dexterity", "This unit's Dexterity."), UnitStats.MinStats.Dexterity);
            UnitStats.MaxStats.Dexterity = EditorGUILayout.IntField(new GUIContent("| Dexterity", "This unit's Dexterity."), UnitStats.MaxStats.Dexterity);

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            UnitStats.MinStats.Intelligence = EditorGUILayout.IntField(new GUIContent("Intelligence", "This unit's Intelligence."), UnitStats.MinStats.Intelligence);
            UnitStats.MaxStats.Intelligence = EditorGUILayout.IntField(new GUIContent("| Intelligence", "This unit's Intelligence."), UnitStats.MaxStats.Intelligence);

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            UnitStats.MinStats.Strength = EditorGUILayout.IntField(new GUIContent("Strength", "This unit's Strength."), UnitStats.MinStats.Strength);
            UnitStats.MaxStats.Strength = EditorGUILayout.IntField(new GUIContent("| Strength", "This unit's Strength."), UnitStats.MaxStats.Strength);

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            UnitStats.MinStats.Vitality = EditorGUILayout.IntField(new GUIContent("Vitality", "This unit's Vitality."), UnitStats.MinStats.Vitality);
            UnitStats.MaxStats.Vitality = EditorGUILayout.IntField(new GUIContent("| Vitality", "This unit's Vitality."), UnitStats.MaxStats.Vitality);

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            UnitStats.MinStats.HpMax = EditorGUILayout.IntField(new GUIContent("Maximum HP", "The maximum amount of hit points this unit can have."), UnitStats.MinStats.HpMax);
            UnitStats.MaxStats.HpMax = EditorGUILayout.IntField(new GUIContent("| Maximum HP", "The maximum amount of hit points this unit can have."), UnitStats.MaxStats.HpMax);
      
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            UnitStats.MinStats.HpRegenRate = EditorGUILayout.IntField(new GUIContent("HP Regen Rate", "The hit points that this unit passively regains per second."), UnitStats.MinStats.HpRegenRate);
            UnitStats.MaxStats.HpRegenRate = EditorGUILayout.IntField(new GUIContent("| HP Regen Rate", "The hit points that this unit passively regains per second."), UnitStats.MaxStats.HpRegenRate);

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            UnitStats.MinStats.MpMax = EditorGUILayout.IntField(new GUIContent("Maximum MP", "The maximum amount of mana points this unit can have."), UnitStats.MinStats.MpMax);
            UnitStats.MaxStats.MpMax = EditorGUILayout.IntField(new GUIContent("| Maximum MP", "The maximum amount of mana points this unit can have."), UnitStats.MaxStats.MpMax);

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            UnitStats.MinStats.MpRegenRate = EditorGUILayout.IntField(new GUIContent("MP Regen Rate", "The mana points that this unit passively regains per second."), UnitStats.MinStats.MpRegenRate);
            UnitStats.MaxStats.MpRegenRate = EditorGUILayout.IntField(new GUIContent("| MP Regen Rate", "The mana points that this unit passively regains per second."), UnitStats.MaxStats.MpRegenRate);

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            UnitStats.MinStats.EnergyMax = EditorGUILayout.IntField(new GUIContent("Maximum Energy", "The maximum amount of energy this unit can have."), UnitStats.MinStats.EnergyMax);
            UnitStats.MaxStats.EnergyMax = EditorGUILayout.IntField(new GUIContent("| Maximum Energy", "The maximum amount of energy this unit can have."), UnitStats.MaxStats.EnergyMax);

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            UnitStats.MinStats.EnergyRegenRate = EditorGUILayout.IntField(new GUIContent("Energy Regen Rate", "The amount of energy this unit passively regains per second."), UnitStats.MinStats.EnergyRegenRate);
            UnitStats.MaxStats.EnergyRegenRate = EditorGUILayout.IntField(new GUIContent("| Energy Regen Rate", "The amount of energy this unit passively regains per second."), UnitStats.MaxStats.EnergyRegenRate);

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            UnitStats.MinStats.RageMax = EditorGUILayout.IntField(new GUIContent("Maximum Rage", "The maximum amount of rage this unit can have."), UnitStats.MinStats.RageMax);
            UnitStats.MaxStats.RageMax = EditorGUILayout.IntField(new GUIContent("| Maximum Rage", "The maximum amount of rage this unit can have."), UnitStats.MaxStats.RageMax);

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();

            UnitStats.MinStats.RageCoolRate = EditorGUILayout.IntField(new GUIContent("Rage Cool Rate", "The rage points that this unit passively loses per second."), UnitStats.MinStats.RageCoolRate);
            UnitStats.MaxStats.RageCoolRate = EditorGUILayout.IntField(new GUIContent("| Rage Cool Rate", "The rage points that this unit passively loses per second"), UnitStats.MaxStats.RageCoolRate);

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();

            UnitStats.MinStats.ExpRequired = EditorGUILayout.IntField(new GUIContent("Experience Required", "The experience points needed to earn this level."), UnitStats.MinStats.ExpRequired);
            UnitStats.MaxStats.ExpRequired = EditorGUILayout.IntField(new GUIContent("| Experience Required", "The experience points needed to earn this level."), UnitStats.MaxStats.ExpRequired);

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
   


            //  if (GUILayout.Button("Edit Level 1 Stats", GUILayout.ExpandWidth(false)))
            //  {
            //      UnitStatSet = UnitStats.MinStats;
            //  }
            //if (GUILayout.Button("Edit Level " + UnitStats.MaxLevel + " Stats", GUILayout.ExpandWidth(false)))
            //  {
            //      UnitStatSet = UnitStats.MaxStats;
            //  }
            ////[MenuItem("Window/Unit Stats Designer")]UnitStatSet.gameObject.name, EditorStyles.boldLabel);

            //    EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(false));

            //    //UnitStatSet.Level = EditorGUILayout.IntField(new GUIContent("Level", "The unit's experience level."), UnitStatSet.Level, GUILayout.ExpandWidth(false));

            //    HL("Base Stats");   

            //    UnitStatSet.Agility = EditorGUILayout.IntField(new GUIContent("Agility", "Agility affects movement speed, physical attack rate and evasion."), UnitStatSet.Agility, GUILayout.ExpandWidth(false));
            //    UnitStatSet.Dexterity = EditorGUILayout.IntField(new GUIContent("Dexterity", "Dexterity affects physical attack accuracy, ranged attack damage and spell cast time."), UnitStatSet.Dexterity, GUILayout.ExpandWidth(false));
            //    UnitStatSet.Intelligence = EditorGUILayout.IntField(new GUIContent("Intelligence", "Intelligence affects offensive spell damage and spell resistance."), UnitStatSet.Intelligence, GUILayout.ExpandWidth(false));

            //    EditorGUILayout.EndHorizontal();
            //    EditorGUILayout.BeginHorizontal();

            //    UnitStatSet.Strength = EditorGUILayout.IntField(new GUIContent("Strength", "Strength affects physical attack damage."), UnitStatSet.Strength, GUILayout.ExpandWidth(false));
            //    UnitStatSet.Vitality = EditorGUILayout.IntField(new GUIContent("Vitality", "Vitality affects resistance to physical damage, max hit points, hit point recovery rate and resistance to physical status effects."), UnitStatSet.Vitality, GUILayout.ExpandWidth(false));

            //    HL("Combat Stats");

            //    UnitStatSet.HpMax = EditorGUILayout.IntField(new GUIContent("Maximum HP", "The maximum amount of hit points this unit can have."), UnitStatSet.HpMax);  
            //    UnitStatSet.HpRegenRate = EditorGUILayout.IntField(new GUIContent("HP Regen Rate", "The hit points that this unit passively regains per second."), UnitStatSet.HpRegenRate);
            //    UnitStatSet.MpMax = EditorGUILayout.IntField(new GUIContent("Maximum MP", "The maximum amount of mana points this unit can have."), UnitStatSet.MpMax);

            //    EditorGUILayout.EndHorizontal();
            //    EditorGUILayout.BeginHorizontal();

            //    UnitStatSet.MpRegenRate = EditorGUILayout.IntField(new GUIContent("MP Regen Rate", "The mana points that this unit passively regains per second."), UnitStatSet.MpRegenRate);
            //    UnitStatSet.EnergyMax = EditorGUILayout.IntField(new GUIContent("Maximum Energy", "The maximum amount of energy this unit can have."), UnitStatSet.EnergyMax);
            //    UnitStatSet.EnergyRegenRate = EditorGUILayout.IntField(new GUIContent("Energy Regen Rate", "The energy this unit passively regains per second."), UnitStatSet.EnergyRegenRate);

            //    EditorGUILayout.EndHorizontal();
        }

        void HL(string header)
        {
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.TextArea("", GUI.skin.horizontalSlider);
            GUILayout.Label(header, EditorStyles.boldLabel);
            EditorGUILayout.BeginHorizontal();
        }
    }
}
