﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace FroggyTools {

    [CustomEditor(typeof(UnitStats))]
    public class UnitStatsEditor : Editor {

        public override void OnInspectorGUI()
        {
            UnitStats UnitStats = (UnitStats)target;

            if (GUILayout.Button("Open Unit Editor")) {
                UnitStatsDesignerWindow Window = (UnitStatsDesignerWindow)EditorWindow.GetWindow(typeof(UnitStatsDesignerWindow));
                Window.minSize = new Vector2(640, 640);
                Window.titleContent.text = "Unit Editor";
                Window.UnitStats = UnitStats;
            }

            EditorGUILayout.LabelField("Events", EditorStyles.boldLabel);
            UnitStats.OnTakeDamageEvent =
                (FroggyTools.Effect)EditorGUILayout.ObjectField(new GUIContent("OnTakeDamage", "This event triggers the attached effect when this unit takes damage."),
                UnitStats.OnTakeDamageEvent,
                typeof(FroggyTools.Effect),
                true);
            
            UnitStats.OnDeathEvent = 
                (FroggyTools.Effect)EditorGUILayout.ObjectField(new GUIContent("OnDeath", "This event triggers the attached effect when this unit dies."), 
                UnitStats.OnDeathEvent, 
                typeof(FroggyTools.Effect), 
                true);

            UnitStats.DestroyOnDeath = EditorGUILayout.Toggle("Destroy On Death", UnitStats.DestroyOnDeath);
        }
    }
}