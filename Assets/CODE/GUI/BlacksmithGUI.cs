﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityCommunityProject.Code;
using DanielQuick;

namespace UnityCommunityProject.GUI
{
    // Gar 04/05/2017 just doing some refactoring
    enum ShopMenuType
    {
        Main = 0,
        Buy = 1,
        Sell = 2,
        Repair = 3
    }

    public class BlacksmithGUI : MonoBehaviour, IInteract
    {
        // hard-wired in the Inspector
        [Header("WIRE-UP MENU REFERENCES")]     // <-- These HeaderAttributes are so darn handy! :)
        public GameObject _mainmenu;
        public GameObject _submenu;
        public Text _submenutext;
        public GameObject[] _submenubuttons;
        public InputInteractTrigger _trigger;

        [Header("SET MAX ITEMS PER PAGE")]
        public int _itemsperpage;

        [Header("ENTER INITIAL INVENTORY HERE")]
        [SerializeField]                        // <-- As is this! :)
        private CollectibleItem[] _startinginventory;



        private readonly List<ICollectibleItem> _blacksmithinventory = new List<ICollectibleItem>();
        private ShopMenuType _ecurShopMenu;
        private int _curpage;


        void Awake()
        {
            // Verifying the hard-wired Editor configuration
            if (_mainmenu == null)
                throw new System.InvalidProgramException("_mainmenu is NULL. Did you forget to wire up the _mainmenu in the Inspector?");

            if (_submenu == null)
                throw new System.InvalidProgramException("_submenu is NULL. Did you forget to wire up the _submenu in the Inspector?");

            if (_submenutext == null)
                throw new System.InvalidProgramException("_submenutext is NULL. Did you forget to wire up the _submenutext in the Inspector?");

            if (_submenubuttons.Length < 1)
                throw new System.InvalidProgramException("_submenubuttons array is empty. Did you forget to wire up the _submenubuttons in the Inspector?");

            if (_trigger == null)
                throw new System.InvalidProgramException("_trigger is NULL. Did you forget to wire up the _trigger in the Inspector?");

            if (_itemsperpage < 1)
                throw new System.InvalidProgramException("_itemsperpage is set to 0. Did you forget to configure the _itemsperpage in the Inspector?");

            if (_submenubuttons.Length < _itemsperpage)
                throw new System.InvalidProgramException("_submenubuttons array is smaller (" + _submenubuttons.Length + " than the number of items to be displayed on this page (" + _itemsperpage + "). Did you forget to wire up the additional _submenubuttons in the Inspector?");


            InitMenu();
            InitPlayerInventory();       // <-- This is here just for testing
            InitBlacksmithInventory();   // <-- This is here just for testing
        }


        public void InitMenu()
        {
            // Gar 04/05/2017 Refactoring to reduce code
            _ecurShopMenu = ShopMenuType.Main;
            HandleMenuDisplay();
        }

        public void BlacksmithInventoryAdd(ICollectibleItem item)
        {
            _blacksmithinventory.Add(item);
        }

        public ICollectibleItem BlacksmithInventoryGet(int id)
        {
            return _blacksmithinventory.Find(i => i.ID == id);
        }

        private void InitBlacksmithInventory()
        {
            // NO LONGER USING THIS KEEPING THESE LINES BECAUSE I AM NOT SURE IF I LIKE THIS APPROACH BETTER OR LOADING IN THE INSPECTOR
            //BlacksmithInventoryAdd(new CollectibleItem { Name = "Short Sword", Type = ItemType.Weapon, Weight = 5, Damage = 10, Armor = 5, BlockChance = 10, NewPrice = 100, SellPrice = 35, MaxDurability = 70 });
            //BlacksmithInventoryAdd(new CollectibleItem { Name = "Long Sword", Type = ItemType.Weapon, Weight = 10, Damage = 20, Armor = 7, BlockChance = 15, NewPrice = 250, SellPrice = 100, MaxDurability = 90 });
            //BlacksmithInventoryAdd(new CollectibleItem { Name = "Leather Armor", Type = ItemType.Armor, Weight = 20, Damage = 0, Armor = 25, BlockChance = 0, NewPrice = 100, SellPrice = 40, MaxDurability = 80 });
            //BlacksmithInventoryAdd(new CollectibleItem { Name = "Chainmail Armor", Type = ItemType.Weapon, Weight = 50, Damage = 0, Armor = 60, BlockChance = 0, NewPrice = 250, SellPrice = 110, MaxDurability = 100 });
            //BlacksmithInventoryAdd(new CollectibleItem { Name = "Small Wooden Shield", Type = ItemType.Shield, Weight = 15, Damage = 0, Armor = 10, BlockChance = 25, NewPrice = 200, SellPrice = 60, MaxDurability = 60 });

            // transfer the initial inventory into the Blacksmith's inventory
            GlobalDataStore gds = GlobalDataStore.Instance;
            gds.BlacksmithInitializeInventory(_startinginventory);
        }

        // this process will likely never be needed in practice. This is purely for testing.
        private void InitPlayerInventory()
        {
            GlobalDataStore gds = GlobalDataStore.Instance;

            //gds.PlayerInventoryAddItem(new CollectibleItem { Name = "Freshly Killed Creeper", Type = ItemType.Misc, Weight = 25, Damage = 0, Armor = 0, BlockChance = 0, NewPrice = 0, SellPrice = 5, MaxDurability = 15 });
            //gds.PlayerInventoryAddItem(new CollectibleItem { Name = "Freshly Killed Creeper", Type = ItemType.Misc, Weight = 25, Damage = 0, Armor = 0, BlockChance = 0, NewPrice = 0, SellPrice = 5, MaxDurability = 15 });
            //gds.PlayerInventoryAddItem(new CollectibleItem { Name = "Wooden Club", Type = ItemType.Weapon, Weight = 2, Damage = 1, Armor = 0, BlockChance = 5, NewPrice = 5, SellPrice = 1, MaxDurability = 25, Durability = 10 });
        }


        #region Main Menu Button Handlers

        public void btnMainMenuBuy_Click()
        {
            // Gar 04/05/2017 Refactoring to reduce code
            _ecurShopMenu = ShopMenuType.Buy;
            HandleMenuDisplay();

            _curpage = 1;
            ShowInventory();
        }

        public void btnMainMenuSell_Click()
        {
            // Gar 04/05/2017 Refactoring to reduce code
            _ecurShopMenu = ShopMenuType.Sell;
            HandleMenuDisplay();

            _curpage = 1;
            ShowInventory();
        }

        public void btnMainMenuRepair_Click()
        {
            // Gar 04/05/2017 Refactoring to reduce code
            _ecurShopMenu = ShopMenuType.Repair;
            HandleMenuDisplay();

            _curpage = 1;
            ShowInventory();
        }

        public void btnMainMenu_Exit()
        {
            NotifyPlayerExitedBlacksmithMenu();
        }

        #endregion


        #region Sub Menu Button Handlers

        public void btnSubMenuItem_Click(int itemIndex)
        {
            Debug.Log(_ecurShopMenu.ToString() + "Menu " + _ecurShopMenu.ToString() + " Item #" + itemIndex.ToString() + " click!");
        }

        public void btnSubMenu_Exit()
        {
            // Gar 04/05/2017 Refactoring to reduce code
            _ecurShopMenu = ShopMenuType.Main;
            HandleMenuDisplay();
        }

        #endregion



        // Gar 04/05/2017 Refactoring to reduce code
        private void HandleMenuDisplay()
        {
            if (_ecurShopMenu == ShopMenuType.Main)
            {
                _submenu.SetActive(false);
                _mainmenu.SetActive(true);
                }
            else
            {
                _mainmenu.SetActive(false);
                _submenu.SetActive(true);

                // Gar 04/06/2017 More refactoring replacing switch statement
                _submenutext.text = "Which item would you like to " + _ecurShopMenu.ToString().ToLower() + "?";
            }
        }

        // Gar 04/05/2017 Refactoring... this returns a list of items for the current menu
        private List<ICollectibleItem> GetInventoryForCurrentMenu()
        {
            List<ICollectibleItem> inventory;
            GlobalDataStore gds = GlobalDataStore.Instance;

            switch (_ecurShopMenu)
            {
                case ShopMenuType.Buy:
                    inventory = gds.BlacksmithGetFullInventory();
                    break;

                case ShopMenuType.Sell:
                    inventory = gds.PlayerGetFullInventory();
                    break;

                case ShopMenuType.Repair:
                    inventory = gds.PlayerGetDamagedInventory();
                    break;

                default:
                    throw new System.NotSupportedException("_eCurShopMenu falls outside the supported Menu Types");
            }

            return inventory;
        }

        // Gar 04/06/2017 Refactoring... this gets the price of the specified item based on the current menu
        private int GetItemPriceForCurrentMenu(ICollectibleItem item)
        {
            int price = 0;
            float durabilityratio = 1.0f;

            switch(_ecurShopMenu)
            {
                case ShopMenuType.Buy:
                    price = item.NewPrice;
                    break;

                case ShopMenuType.Sell:
                    if (item.MaxDurability > 0 && item.Durability > 0)
                    {
                        durabilityratio = item.MaxDurability / item.Durability;
                        price = (int)(item.SellPrice * durabilityratio);
                    }
                    else
                        price = 1;      // temporary for testing
                    break;

                case ShopMenuType.Repair:
                    price = item.NewPrice / 2;
                    break;

                default:
                    throw new System.NotSupportedException("_eCurShopMenu falls outside the supported Menu Types");
            }

            return price;
        }

        // Gar 04/06/2017 Refactored to combine ShowMerchantInventory, ShowPlayerDamagedInventory() with ShowPlayerInventory()
        private void ShowInventory()
        {
            GlobalDataStore gds = GlobalDataStore.Instance;
            List<ICollectibleItem> inventory = GetInventoryForCurrentMenu();
            bool bNoItems = false;

            if (inventory == null)
                bNoItems = true;
            else if (inventory.Count < 1)
                bNoItems = true;

            if (bNoItems)
            {
                _submenutext.text = "You have no items to sell.";

                for (int index = 0; index < _itemsperpage; index++)
                    _submenubuttons[index].SetActive(false);

                return;
            }

            int indexoffset = (_curpage * _itemsperpage) - _itemsperpage;
            int numitems = (inventory.Count >= _itemsperpage) ? _itemsperpage : inventory.Count;
            int price;
            int totalprice;

            Text text;
            for (int index = 0; index < _itemsperpage; index++)
            {
                // has this _submenubuttons Button reference been wired up in the Inspector?
                if (_submenubuttons[index] != null)
                {
                    // does this index correspond to an item that should be represented by a button?
                    if (index < numitems)
                    {
                        _submenubuttons[index].SetActive(true);
                        text = _submenubuttons[index].GetComponentInChildren<Text>();

                        if (text != null)
                        {
                            price = GetItemPriceForCurrentMenu(inventory[indexoffset + index]);
                            totalprice = price * inventory[indexoffset + index].Quantity;
                            text.text = "(" + inventory[indexoffset + index].Quantity + ") " + inventory[indexoffset + index].Name + " -- " + price + " gold (" + totalprice + " total)";
                        }
                        else
                            throw new System.NullReferenceException("Failed to find Text for btnItem" + (index + 1).ToString());

                    }
                    // no... then hide it
                    else
                    {
                        _submenubuttons[index].SetActive(false);
                    }
                }
                else
                {
                    throw new System.NullReferenceException("_submenubuttons[" + index + "] is NULL. Did you forget to wire up the _submenubuttons in the Inspector?");
                }
            }

            inventory = null;
        }

        private void NotifyPlayerExitedBlacksmithMenu()
        {
            IMessageReceiver receiver = GameObject.Find("Player").GetComponent<IMessageReceiver>();
            if (receiver == null)
                return;

            // create the message object & populate the descriptors information
            Message message = new Message();
            message.mtype = MessageType.PlayerNoLongerBusy;

            // send message
            receiver.ReceiveMessage(message);

            // Gar 04/05/2017 Implemented DanielQuick's IInteraction method for consistency.
            _trigger.ResetTrigger();
            this.gameObject.SetActive(false);
        }


        // Gar 04/05/2017
        #region IInteract Implementation

        public void Interact()
        {
            this.gameObject.SetActive(true);

            IMessageReceiver receiver = GameObject.Find("Player").GetComponent<IMessageReceiver>();
            if (receiver == null)
                return;

            // Notify PlayerControl the player is now busy
            Message message = new Message();
            message.mtype = MessageType.PlayerIsBusy;

            // send message
            receiver.ReceiveMessage(message);
        }

        public void EndInteract()
        {
        }

        #endregion
    }
}
